<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Ada bojana - Administracija</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin=""/>

</head>
<body class="hold-transition sidebar-mini layout-fixed">


<div class="wrapper">

    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="/" class="nav-link">Početna</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fas fa-th-large"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="fas fa-power-off"></i> Odjavi se</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>

        </ul>
    </nav>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <a href="/dashboard" class="brand-link">
            <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">Ada <b>Bojana</b></span>
        </a>

        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                </div>
            </div>

            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">

                    @if(Auth::user()->role->name == 'owner' || Auth::user()->role->name == 'admin' || Auth::user()->role->name == 'head')
                        <li class="nav-header ml-3"> OPCIJE</li>
                        <li class="nav-item">
                            <a href="/my-houses" class="nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p>
                                    Moje kuće
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/my-requests" class="nav-link">
                                <i class="nav-icon fas fa-mail-bulk"></i>
                                <p>
                                    Rezervacije
                                    @if($reservations > 0)
                                        <span class="right badge badge-danger">{{$reservations}}</span>
                                    @endif
                                </p>
                            </a>
                        </li>
                        <li class="nav-item" style="pointer-events: none; opacity: 0.6;">
                            <a href="/my-report" class="nav-link" disabled="">
                                <i class="nav-icon fas fas fa-copy"></i>
                                <p>
                                    Statistika
                                    <span class="right badge badge-danger">Uskoro</span>
                                </p>
                            </a>
                        </li>
                    @endif
                    @if(Auth::user()->role->name == 'admin' || Auth::user()->role->name == 'head')
                        <li class="nav-header">ADMIN OPCIJE</li>
                        <li class="nav-item">
                            <a href="/admin/houses" class="nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p>
                                    Pregled kuća
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/admin/messages" class="nav-link">
                                <i class="nav-icon fas fa-mail-bulk"></i>
                                <p>
                                    Kontakt poruke
                                    @if($messages > 0)
                                        <span class="right badge badge-danger">{{$messages}}</span>
                                    @endif
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/admin/requests" class="nav-link">
                                <i class="nav-icon fas fa-mail-bulk"></i>
                                <p>
                                    Zahtjevi za rezervaciju
                                </p>
                            </a>
                        </li>
                        <li class="nav-item" style="pointer-events: none; opacity: 0.6;">
                            <a href="/admin/stats" class="nav-link">
                                <i class="nav-icon fas fa-list"></i>
                                <p>
                                    Izvještaj
                                    <span class="right badge badge-danger">Uskoro</span>

                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/admin/owners" class="nav-link">
                                <i class="nav-icon fas fa-user-alt"></i>
                                <p>
                                    Vlasnici
                                </p>
                            </a>
                        </li>
                    @endif
                    @if(Auth::user()->role->name == 'head')
                        <li class="nav-header">HEAD ADMIN OPCIJE</li>
                        <li class="nav-item" style="pointer-events: none; opacity: 0.6;">
                            <a href="head/stats" class="nav-link">
                                <i class="nav-icon fas fa-list"></i>
                                <p>
                                    Detaljni izvještaj
                                    <span class="right badge badge-danger">Uskoro</span>
                                </p>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </aside>

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        @yield('section-button')
                        <h1 class="m-0 text-dark">@yield('section-name')</h1>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="div-loader" id="loader">
                    <div class="loader"></div>
                </div>
                @yield('content')
            </div>
        </section>
    </div>
    <footer class="main-footer">
        <strong>Copyright &copy; <a href="http://adminlte.io">Ada Bojana</a>.</strong>
        All rights reserved.

    </footer>

    <aside class="control-sidebar control-sidebar-dark">
    </aside>
</div>


<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('js/loader.js')}}"></script>

<!-- jQuery UI 1.11.4 -->
<script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
        integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
        crossorigin=""></script>

<!-- Sparkline -->
<script src="{{asset('plugins/sparklines/sparkline.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="{{asset('js/helpers.js')}}"></script>
@yield('scripts')
</body>
</html>
