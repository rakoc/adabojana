<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Platforma za rezervaciju kuća na Adi Bojani">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="keywords" content="Azenta, Ada, Bojana, kuce, kucice, kućice, more">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Azenta | Ada Bojana</title>


    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap"
          rel="stylesheet">


    <link rel="stylesheet" href="{{asset('azenta/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('azenta/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('azenta/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('azenta/css/themify-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('azenta/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('azenta/css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('azenta/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('azenta/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('azenta/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('azenta/css/style.css')}}" type="text/css">
</head>

<body>

<div id="preloder">
    <div class="loader"></div>
</div>

<div class="offcanvas-menu-overlay"></div>
<div class="canvas-open">
    <i class="icon_menu"></i>
</div>
<div class="offcanvas-menu-wrapper">
    <div class="canvas-close">
        <i class="icon_close"></i>
    </div>
    <div class="language-bar">
        <div class="language-option">
            <span>{{strtoupper(\Illuminate\Support\Facades\Session::get('locale'))}}</span>
            <i class="fa fa-angle-down"></i>
            <div class="flag-dropdown">
                <ul>
                    <li><a href="/set-locale/en">EN</a></li>
                    <li><a href="/set-locale/cg">CG</a></li>
                </ul>
            </div>
        </div>
        <div class="property-btn">
            <a href="#" class="property-sub">@lang('trans.send-query')</a>
        </div>
    </div>
    <nav class="main-menu">
        <ul class="nav-custom">
            <li><a href="/">@lang('trans.nav-home')</a></li>
            <li><a href="/houses">@lang('trans.nav-house')</a></li>
            <li><a href="/contact-us">@lang('trans.nav-contact')</a></li>
        </ul>
    </nav>
    <div class="nav-logo-right">
        <ul>
            <li>
                <i class="icon_phone"></i>
                <div class="info-text">
                    <span>@lang('trans.nav-phone'):</span>
                    <p>(+12) 345 6789</p>
                </div>
            </li>
            <li>
                <i class="icon_mail"></i>
                <div class="info-text">
                    <span>Email:</span>
                    <p>Info.cololib@gmail.com</p>
                </div>
            </li>
        </ul>
    </div>
</div>

<header class="header-section header-normal">
    <div class="top-nav">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <nav class="main-menu">
                        <ul class="nav-custom">
                            <li class="active"><a href="/">@lang('trans.nav-home')</a></li>
                            <li><a href="/houses">@lang('trans.nav-house')</a></li>
                            <li><a href="/contact-us">@lang('trans.nav-contact')</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-5">
                    <div class="top-right">
                        <div class="language-option">
                            <span>{{strtoupper(\Illuminate\Support\Facades\Session::get('locale'))}}</span>
                            <i class="fa fa-angle-down"></i>
                            <div class="flag-dropdown">
                                <ul>
                                    <li><a href="/set-locale/en">EN</a></li>
                                    <li><a href="/set-locale/cg">CG</a></li>
                                </ul>
                            </div>
                        </div>
                        <a href="#" class="property-sub">@lang('trans.send-query')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nav-logo">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="logo">
                        <a href="/"><img src="{{asset('azenta/img/logo.png')}}" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="nav-logo-right">
                        <ul>
                            <li>
                                <i class="icon_phone"></i>
                                <div class="info-text">
                                    <span>@lang('trans.nav-phone'):</span>
                                    <p>(+12) 345 789</p>
                                </div>
                            </li>
                            <li>
                                <i class="icon_mail"></i>
                                <div class="info-text">
                                    <span>Email:</span>
                                    <p>Info.cololib@gmail.com</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('content')

<footer class="footer-section set-bg" style="background-size: cover;" data-setbg="{{asset('ada-bojana.jpg')}}">
    <div class="container">
        <div class="footer-text">
            <div class="row">
                <div class="col-lg-4">
                    <div class="footer-logo">
                        <div class="logo">
                            <a href="#"><img src="{{asset('azenta/img/footer-logo.png')}}" alt=""></a>
                        </div>
                        <p>@lang('trans.newsletter')</p>
                        <form action="#" class="newslatter-form">
                            <input type="text" placeholder="Email">
                            <button type="submit"><i class="fa fa-location-arrow"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="footer-widget">
                        <h4>@lang('trans.social-media')</h4>
                        <ul class="social">
                            <li><i class="ti-facebook"></i> <a href="#">Facebook</a></li>
                            <li><i class="ti-instagram"></i> <a href="#">Instagram</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="footer-widget">
                        <h4>@lang('trans.contact')</h4>
                        <ul class="contact-option">
                            <li><i class="fa fa-phone"></i> (+88) 666 121 4321</li>
                            <li><i class="fa fa-envelope"></i> info.colorlib@gmail.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-text">
            <p>
                Copyright &copy;
                <script>document.write(new Date().getFullYear());</script>
                All rights reserved.
            </p>
        </div>
    </div>
</footer>

<!-- Js Plugins -->
<script src="{{asset('azenta/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('azenta/js/bootstrap.min.js')}}"></script>
<script src="{{asset('azenta/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('azenta/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('azenta/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('azenta/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('azenta/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('azenta/js/main.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="{{asset('js/front.js')}}"></script>

</body>

</html>
