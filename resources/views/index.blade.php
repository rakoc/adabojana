@extends('layouts.front')
@section('content')
    <section class="hero-section">
        <div class="hero-items owl-carousel">

        @if($slider && sizeof($slider) > 0)
            @foreach($slider as $s)
            <div class="single-hero-item set-bg" data-setbg="{{asset($s->image)}}">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <div class="hero-text">
                                <p class="room-location"><i class="icon_pin"></i>{{$s->address}}</p>
                                <h2>
                                    @if(session('locale') == 'en')
                                        {{$s->translations[0]->name}}
                                        @else
                                        {{$s->name}}
                                    @endif
                                </h2>
                                <div class="room-price">
                                    <span>@lang('trans.price'):</span>
                                    <p>{{$s->price}}&euro;</p>
                                </div>
                                <ul class="room-features">
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <p>{{$s->guests}} Guests</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>{{$s->rooms}} Bedrooms</p>
                                    </li>
{{--                                    <li>--}}
{{--                                        <i class="fa fa-wifi"></i>--}}
{{--                                        <p>Free WIFI</p>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <i class="fa fa-car"></i>--}}
{{--                                        <p>Free Parking</p>--}}
{{--                                    </li>--}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
        </div>
        <div class="thumbnail-pic" style="display: none;">
            <div class="thumbs owl-carousel">

            @if($slider && sizeof($slider) > 0)
                @foreach($slider as $s)
                <div class="item">
                    <img src="{{asset($s->image)}}" alt="">
                </div>
                @endforeach
            @endif
            </div>
        </div>
    </section>


    <div class="search-form">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-form-text">
                        <div class="search-text">
                            <i class="fa fa-search"></i>
                            @lang('trans.search-houses')
                        </div>
                        <div class="home-text">
                        </div>
                    </div>
                    <form action="/search-all" class="filter-form" method="POST">
                        @csrf
                        <div class="first-row">
                            <input type="text" class="custom-input-field" placeholder="@lang('trans.ph-name')">
                        </div>
                        <div class="second-row">
                            <input type="number" class="short-custom-input-field" placeholder="@lang('trans.ph-guest')">
                            <div class="price-range-wrap">
                                <div class="price-text">
                                    <label for="priceRange">@lang('trans.ph-price'):</label>
                                    <input type="text" id="priceRange" readonly style="width: 100px">
                                </div>
                                <div id="price-range" class="slider"></div>
                            </div>
                            <div class="price-range-wrap">
                                <div class="price-text">
                                    <label for="roomsizeRange">@lang('trans.ph-rooms'):</label>
                                    <input type="text" id="roomsizeRange" readonly style="width: 50px">
                                </div>
                                <div id="roomsize-range" class="slider"></div>
                            </div>
                            <button type="button" class="search-btn">@lang('trans.button-search')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <section class="howit-works spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <span>@lang('trans.find-your-house')</span>
                        <h2>@lang('trans.how-it-works')</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-howit-works">
                        <img src="{{asset('azenta/img/howit-works/howit-works-1.png')}}" alt="">
                        <h4>@lang('trans.icon-search')</h4>
                        <p>@lang('trans.icon-search-desc')</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-howit-works">
                        <img src="{{asset('azenta/img/howit-works/howit-works-2.png')}}" alt="">
                        <h4 class="pt-2">@lang('trans.icon-find')</h4>
                        <p>@lang('trans.icon-find-desc')</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-howit-works">
                        <img src="{{asset('azenta/img/howit-works/howit-works-3.png')}}" alt="">
                        <h4>@lang('trans.icon-contact')</h4>
                        <p>@lang('trans.icon-contact-desc')</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="top-properties-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="properties-title">
                        <div class="section-title">
                            <span>@lang('trans.top-rang')</span>
                            <h2>@lang('trans.top-choice')</h2>
                        </div>
                        <a href="#" class="top-property-all">@lang('trans.button-view-all')</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="top-properties-carousel owl-carousel">
                <div class="single-top-properties">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="stp-pic">
                                <img src="{{asset('azenta/img/properties/properties-1.jpg')}}" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="stp-text">
                                <div class="s-text">For Sale</div>
                                <h2>Villa 9721 Glen Creek</h2>
                                <div class="room-price">
                                    <span>Start From:</span>
                                    <h4>$3.000.000</h4>
                                </div>
                                <div class="properties-location"><i class="icon_pin"></i> 9721 Glen Creek Ave. Ballston Spa,
                                    NY
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                    ut labore et dolore magna aliqua.</p>
                                <ul class="room-features">
                                    <li>
                                        <i class="fa fa-arrows"></i>
                                        <p>5201 sqft</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>8 Bed Room</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-bath"></i>
                                        <p>7 Baths Bed</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>1 Garage</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-top-properties">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="stp-pic">
                                <img src="{{asset('azenta/img/properties/properties-2.jpg')}}" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="stp-text">
                                <div class="s-text">For Sale</div>
                                <h2>Villa 9721 Glen Creek</h2>
                                <div class="room-price">
                                    <span>Start From:</span>
                                    <h4>$3.000.000</h4>
                                </div>
                                <div class="properties-location"><i class="icon_pin"></i> 9721 Glen Creek Ave. Ballston Spa,
                                    NY
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                    ut labore et dolore magna aliqua.</p>
                                <ul class="room-features">
                                    <li>
                                        <i class="fa fa-arrows"></i>
                                        <p>5201 sqft</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>8 Bed Room</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-bath"></i>
                                        <p>7 Baths Bed</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>1 Garage</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
