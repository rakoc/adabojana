@extends('layouts.front')
@section('content')
    <section class="contact-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row text-center" >
                        <div class="col-lg-5 text-center" style="margin: 0 auto;">
                            <div class="contact-text">
                                <div class="section-title">
                                    <span>@lang('trans.contact')</span>
                                    <h2>@lang('trans.contact-title')</h2>
                                </div>
                                <form action="#" class="contact-form" id="contact-form">
                                    <input type="hidden" name="message_status_id" value="1">
                                    <input type="text" name="name" placeholder="@lang('trans.contact-name')">
                                    <input type="text" name="email" placeholder="Email">
                                    <input type="text" name="phone_number" placeholder="@lang('trans.contact-phone')">
                                    <textarea name="message" placeholder="@lang('trans.contact-message')"></textarea>
                                    <button type="button" onclick="sendContact()" class="site-btn" >@lang('trans.contact-send-message')</button>
                                </form>
                                <hr>
                                <p>@lang('trans.contact-desc')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection
