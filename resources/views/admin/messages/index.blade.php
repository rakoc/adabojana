@extends('layouts.admin')

@section('section-name')
    Poruke
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Datum</th>
                            <th class="text-center">Ime</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Telefon</th>
                            <th class="text-center">Status poruke</th>
                            <th class="text-center">Akcije</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($messages && sizeof($messages) > 0)
                            @foreach($messages as $m)
                                <tr>
                                    <td class="text-center">#{{$m->id}}</td>
                                    <td class="text-center">{{Carbon\Carbon::createFromTimeStamp(strtotime($m->created_at))->diffForHumans()}}</td>
                                    <td class="text-center">{{$m->name}}</td>
                                    <td class="text-center">{{$m->email}}</td>
                                    <td class="text-center">{{$m->phone_number}}</td>
                                    <td class="text-center">
                                        @if($m->messageStatus->code == 'WAITING')
                                            <span class="badge badge-warning">Na čekanju</span>
                                        @elseif($m->messageStatus->code == 'SEEN')
                                            <span class="badge badge-secondary">Vidjeno</span>
                                        @elseif($m->messageStatus->code == 'REPLIED')
                                            <span class="badge badge-primary">Odgovoreno</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-primary" href="/admin/see-message/{{$m->id}}">Otvori</a></td>
                                </tr>
                            @endforeach

                        @else
                            <tr>
                                <td colspan="7" class="text-center">
                                    <p><b>Trenutno nema poruka</b></p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{ $messages->links() }}
@endsection
