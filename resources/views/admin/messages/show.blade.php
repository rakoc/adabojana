@extends('layouts.admin')

@section('section-name')
    Pregled poruke
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Informacije</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="messageStatus">Status poruke:</label>
                            <p class="messageStatus">
                                @if($message->messageStatus->code == 'SEEN')
                                    <span class="badge badge-secondary">Vidjeno</span>
                                @elseif($message->messageStatus->code == 'REPLIED')
                                    <span class="badge badge-primary">Odgovoreno</span>
                                @endif
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="date">Datum:</label>
                            <p id="date">
                                {{ Carbon\Carbon::createFromTimeStamp(strtotime($message->created_at))->diffForHumans() }}
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="name">Ime:</label>
                            <p id="name">{{$message->name}}</p>
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <p id="email">{{$message->email}}</p>
                        </div>
                        <div class="form-group">
                            <label for="phone">Broj telefona:</label>
                            <p class="phone">{{$message->phone_number}}</p>
                        </div>
                        <div class="form-group">
                            <label for="message">Poruka:</label>
                            <p class="message">{{$message->message}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Odgovor</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="messageReply">Tekst poruke:</label>
                            <textarea name="messageReply" id="messageReply" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="languageMessage">Jezik poruke:</label>
                            <select name="languageMessage" id="languageMessage" class="form-control">
                                <option value="" disabled selected>Izaberi jezik</option>
                                <option value="mne" >Crnogorski jezik</option>
                                <option value="eng" >Engleski jezik</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-success float-right" type="button"><i class="fas fa-envelope"></i> Odgovori</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
