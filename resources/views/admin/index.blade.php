@extends('layouts.admin')
@section('section-name')
    Dashboard
@endsection

@section('content')
    @if(sizeof(Auth::user()->house) > 0 && Auth::user()->role->name == 'owner')
        <div class="row text-center">
            <h1 style="margin: 0 auto; font-size: 100px">Dobrodošli</h1><br>
        </div>
        <div class="row">
            <p style="margin: 0 auto"><i class="fas fa-arrow-left"></i> Upravljajte vašim sadržajem kroz lijevi navigacioni meni.</p>
        </div>
    @elseif(Auth::user()->role->name == 'admin')
        <div class="row text-center">
            <h1 style="margin: 0 auto; font-size: 100px">Dobrodošli</h1><br>
        </div>
        <div class="row">
            <p style="margin: 0 auto"><i class="fas fa-arrow-left"></i> Upravljajte sadržajem platforme kroz lijevi navigacioni meni</p>
        </div>
    @elseif(Auth::user()->role->name == 'head')
        <div class="row text-center">
            <h1 style="margin: 0 auto; font-size: 100px">Dobrodošli</h1><br>
        </div>
        <div class="row">
            <p style="margin: 0 auto"><i class="fas fa-arrow-left"></i> Upravljajte sadržajem platforme kroz lijevi navigacioni meni</p>
        </div>

    @endif
    @endsection
