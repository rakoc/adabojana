@extends('layouts.admin')

@section('section-button')
    @if(auth()->user()->role->name != 'owner' && auth()->user()->role->name != 'user')
        <a class="btn btn-primary float-right" href="/admin/house"><i class="fas fa-plus"></i> Dodaj kuću</a>
    @endif
@endsection

@section('section-name')
    @if(Route::current()->uri())
        Pregled kuća
    @else
        Moje kuće
    @endif
    {{--    <button class="btn btn-success float-right"><i class="fas fa-plus"></i> Dodaj kuću</button>--}}
@endsection

@section('content')

    @if($houses && sizeof($houses) > 0)
        @foreach($houses as $house)
            <div class="card card-solid">
                <div class="card-body pb-0">
                    <div class="row d-flex align-items-stretch">
                        <div class="col-12 ">
                            <div class="card bg-light">
                                <div class="card-header text-muted border-bottom-0">
                                    Kuća #{{$house->id}}
                                </div>
                                <div class="card-body pt-0">
                                    <div class="row">
                                        <div class="col-9">
                                            <h2 class="lead"><b>{{$house->name}}</b></h2>
                                            <p class="text-muted text-sm"><b>Adresa: </b> {{$house->address}} </p>
                                            <ul class="ml-4 mb-0 fa-ul text-muted">
                                                <li>{!! $house->description !!}</li>
                                            </ul>
                                        </div>
                                        <div class="col-3 text-center">
                                            <img src="{{asset($house->image)}}" alt="" class="img-fluid" width="200">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-right">
                                        <a @if(Route::current()->uri() == 'admin/houses')
                                           href="/admin/houses/{{$house->id}}/edit"
                                           @else
                                           href="/change-my-house/{{$house->id}}"
                                           @endif
                                           class="btn btn-sm btn-primary">
                                            <i class="fas fa-pen"></i> Izmijeni
                                        </a>
                                        <button href="#" class="btn btn-sm bg-danger"
                                                @if(Route::current()->uri() == 'admin/houses')
                                                onclick="swalDeleteHouse('/delete-house', {{$house->id}})"
                                                @else
                                                onclick="swalDeleteHouse('admin/delete-house', {{$house->id}})"
                                            @endif >
                                            <i class="fa fa-times"></i> Deaktiviraj
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ $houses->links() }}
                    </div>

                </div>
            </div>
        @endforeach

    @else
        <div class="card card-solid">
            <div class="card-body pb-0">
                <div class="row d-flex align-items-stretch">
                    <div class="col-12 text-center">
                        <p>Trenutno nemate kuća</p>
                    </div>
                </div>
            </div>
        </div>

    @endif
@endsection
