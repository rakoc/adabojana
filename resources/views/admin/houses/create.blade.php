@extends('layouts.admin')

@section('section-name')
    Kreiranje kuće
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Informacije</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="houseId" id="houseId">
                        <div class="form-group">
                            <label for="" class="houseOwner">Vlasnik:</label>
                            <select name="houseOwner" id="owner" class="form-control">
                                <option value="0" disabled selected>Izaberi vlasnika</option>
                                @if($users && sizeof($users) > 0)
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="houseName">Naziv:</label>
                            <input type="text" name="name" class="form-control" placeholder="Unesite naziv kuće"
                                   id="houseName" required>
                        </div>
                        <div class="form-group">
                            <label for="housePrice">Cijena po noćenju (&euro;):</label>
                            <input type="number" name="price" id="housePrice"
                                   class="form-control" placeholder="Unesite cijenu" required>
                        </div>
                        <div class="form-group">
                            <label for="houseRooms">Broj soba: *</label>
                            <input type="number" name="rooms" placeholder="Unesite broj soba" id="houseRooms" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="houseGuests">Max. broj gosti: *</label>
                            <input type="number" name="guests"  placeholder="Unesite broj gosti" id="houseGuests" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="houseAddress">Adresa:</label>
                            <input type="text" name="address" placeholder="Unesite adresu" id="houseAddress" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="houseDescription">Kratki opis:</label>
                            <textarea type="text" name="description" placeholder="Unesite kratki opis" id="houseDescription" class="form-control textarea"
                                      cols="30" rows="30" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="houseImage">Naslovna slika:</label><br>
                            <input type="file" class="custom-file" id="coverImage" required>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="message">Više slika:</label>
                            <input type="file" name="multiImage" id="multiImage" class="custom-file" multiple required>
                        </div>
                    </div>
                </div>

                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title">Sadržaj</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        @if($attributes && sizeof($attributes) > 0)
                            @foreach($attributes as $attribute)
                                <div class="form-group">
                                    <label
                                        for="{{$attribute->name}}">{{$attribute->translations->where('language_id', '1')->first()->name}}
                                        :</label>
                                    <input type="checkbox" name="check-{{$attribute->name}}" value="true">
                                    <input type="text" name="text-{{$attribute->name}}" class="form-control" value=""
                                           placeholder="Dodatno (opciono)">
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-6">

                @if($languages && sizeof($languages)  > 0)
                    @foreach($languages as $lang)
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Prevod - {{$lang->name}}</h3>
                                <input type="hidden" id="transId">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="translationName-{{$lang->code}}">Naziv kuće:</label>
                                    <input name="translationName-{{$lang->code}}" placeholder="Unesite prevod naziva kuće" id="translationName-{{$lang->code}}"
                                           type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="translationDescription-{{$lang->code}}">Kratki opis:</label>
                                    <textarea name="translationDescription"
                                              id="translationDescription-{{$lang->code}}" placeholder="Unesite prevod opisa kuće" class="textarea"
                                              cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @endif

                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Mapa</h3>
                            <div class="card-tools">
                                <button class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body text-center">
                            <div id="mapid" style="height: 500px"></div>
                            <input type="hidden" id="mapLat" value="">
                            <input type="hidden" id="mapLng" value="">
                        </div>
                    </div>
            </div>
        </div>
        <button class="btn btn-success mb-2" onclick="createHouse()">
            <i class="fa fa-check"></i> Sačuvaj
        </button>
    </section>

@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('.textarea').summernote();
            var latH = $('#mapLat').val();
            var lngH = $('#mapLat').val();
            if (latH !== '' && lngH !== '') {
                var mymap = L.map('mapid').setView([latH, $('#mapLng').val()], 13);
            } else {
                var mymap = L.map('mapid').setView([41.863097074049186, 19.360124695975838], 13);
            }

            L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoicmFrb2MiLCJhIjoiY2p5Z2lpZnJzMDFjajNjbnNrNWtyMGdhayJ9.joCYYhr4sVMAZGvLsSp63Q', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1,
                accessToken: 'pk.eyJ1IjoicmFrb2MiLCJhIjoiY2p5Z2lpZnJzMDFjajNjbnNrNWtyMGdhayJ9.joCYYhr4sVMAZGvLsSp63Q'
            }).addTo(mymap);

            var marker = new L.marker(new L.LatLng($('#mapLat').val(), $('#mapLng').val()));
            mymap.addLayer(marker);

            function addPin(e) {
                if (marker) {
                    mymap.removeLayer(marker);
                }
                marker = new L.marker(e.latlng, {draggable: true});
                mymap.addLayer(marker);
                $('#mapLat').val(marker.getLatLng().lat);
                $('#mapLng').val(marker.getLatLng().lng);
                console.log(e.latlng);
            }

            mymap.on('click', addPin);


            function setupAjax() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }


        });

        function createHouse() {

            var data = new FormData;
            var image = $('#coverImage')[0].files[0];
            let multiImages = $('#multiImage')[0];
            if ($('#owner :selected').val() != '0') {
                data.append('owner', $('#owner').val());
                console.log($('#owner :selected').val())
            }
            data.append('id', $('#houseId').val());
            data.append('houseName', $('#houseName').val());
            data.append('housePrice', $('#housePrice').val());
            data.append('houseAddress', $('#houseAddress').val());
            data.append('houseRooms', $('#houseRooms').val());
            data.append('houseGuests', $('#houseGuests').val());
            data.append('houseDescription', $('#houseDescription').summernote('code'));
            data.append('lat', $('#mapLat').val());
            data.append('lng', $('#mapLng').val());
            if (typeof image != 'undefined') {
                data.append('coverImage', image);
            }
            if (multiImages.files.length > 0) {
                for (var i = 0; i < multiImages.files.length; i++) {
                    data.append('images[]', multiImages.files[i]);
                }
            }
            var elems = $(':input[name^="check-"]');
            for (var j = 0; j < elems.length; j++) {
                var name = 'text-' + elems[j].name.split('-')[1];
                var part = [elems[j].name.split('-')[1], $(':input[name^="check-' + elems[j].name.split('-')[1] + '"]:checked').length > 0 ? 'true' : 'false', $("input[name^='" + name + "']").val()];
                data.append('attributes[]', JSON.stringify(part));
            }
            var languages = [];
            var transNames = $(':input[name^="translationName-"]');
            for (var k = 0; k < transNames.length; k++) {
                languages.push(transNames[k].name.split('-')[1]);
            }

            for (var z = 0; z < languages.length; z++) {

                var transName = $('#translationName-' + languages[z]).val();
                var transDescription = $('#translationDescription-' + languages[z]).val();

                var trans = [languages[z], transName, transDescription];
                data.append('translations[]', JSON.stringify(trans));
            }
            console.log(data)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/admin/houses/create',
                method: 'post',
                data: data,
                contentType: false,
                processData: false
            })
                .done(function () {
                    toast('success', 'Uspješno ste kreirali kuću');
                    setTimeout(function () {
                        location.href = '/admin/houses'
                    }, 2000);
                })
                .fail(function (response) {
                    var message = JSON.parse(response.responseText).errors;
                    toast('error', String(message[Object.keys(message)[0]]));
                })
        }
    </script>

@endsection
