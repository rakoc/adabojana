@extends('layouts.admin')

@section('section-name')
    Rezervacije
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Datum</th>
                            <th class="text-center">Ime</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Telefon</th>
                            <th class="text-center">Kuća</th>
                            <th class="text-center">Status zahtjeva</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($requests && sizeof($requests) > 0)
                            @foreach($requests as $r)
                                <tr>
                                    <td class="text-center">#{{$r->id}}</td>
                                    <td class="text-center">{{Carbon\Carbon::createFromTimeStamp(strtotime($r->created_at))->diffForHumans()}}</td>
                                    <td class="text-center">{{$r->name}}</td>
                                    <td class="text-center">{{$r->email}}</td>
                                    <td class="text-center">{{$r->phone_number}}</td>
                                    <td class="text-center">{{$r->house->name}}</td>
                                    <td class="text-center">
                                        @if($r->requestStatus->code == 'WAITING')
                                            <span class="badge badge-warning">Na čekanju</span>
                                        @elseif($r->requestStatus->code == 'SEEN')
                                            <span class="badge badge-secondary">Vidjeno</span>
                                        @elseif($r->requestStatus->code == 'REPLIED')
                                            <span class="badge badge-primary">Odgovoreno</span>
                                        @elseif($r->requestStatus->code == 'APPROVED')
                                            <span class="badge badge-success">Odobreno</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-primary"
                                           @if(Route::current()->uri() == 'admin/requests') href="/admin/see-request/{{$r->id}}"
                                           @else href="/see-request/{{$r->id}}" @endif>Otvori</a></td>
                                </tr>
                            @endforeach

                        @else
                            <tr>
                                <td colspan="7" class="text-center">
                                    <p><b>Trenutno nemate rezervacija</b></p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if($requests != null)
        {{ $requests->links() }}
    @endif
@endsection
