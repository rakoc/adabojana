@extends('layouts.admin')

@section('section-name')
    Izmjena vlasnika
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-6" style="margin: 0 auto;">
                <div class="card card-primary">
                    <div class="card-header">
                        Izmjena vlasnika
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name" class="">Ime i prezime *</label>
                            <input type="text" name="name" id="name" required class="form-control"
                                   placeholder="Unesite ime i prezime" value="{{$owner->name}}">
                        </div>
                        <div class="form-group">
                            <label for="name" class="">Email *</label>
                            <input type="email" name="email" id="email" required class="form-control"
                                   placeholder="Unesite email" value="{{$owner->email}}">
                        </div>
                        <div class="form-group">
                            <label for="name" class="">Broj telefona *</label>
                            <input type="tel" name="phone" id="phone" required class="form-control"
                                   placeholder="Unesite broj telefona" value="{{$owner->phone_number}}">
                        </div>
                        <div class="form-group">
                            <label for="name" class="">Lozinka</label>
                            <input type="password" name="password" id="password" required class="form-control"
                                   placeholder="Unesite novu lozinku">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-success" onclick="updateOwner({{$owner->id}})"><i class="fas fa-save"></i> Sačuvaj</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{asset('js/functions.js')}}"></script>
@endsection
