@extends('layouts.admin')

@section('section-button')
    <a class="btn btn-primary float-right" href="/admin/owners/create"><i class="fas fa-plus"></i> Dodaj vlasnika</a>
@endsection
@section('section-name')
    Vlasnici
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Ime i prezime</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Broj telefona</th>
                            <th class="text-center">Uloga</th>
                            <th class="text-center">Akcije</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($owners && sizeof($owners) > 0)
                            @foreach($owners as $owner)
                                <tr>
                                    <td class="text-center">#{{$owner->id}}</td>
                                    <td class="text-center">{{$owner->name}}</td>
                                    <td class="text-center">{{$owner->email}}</td>
                                    <td class="text-center">{{$owner->phone_number}}</td>
                                    <td class="text-center">{{$owner->role->name}}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary" href="/admin/owners/{{$owner->id}}/update">
                                            <i class="fas fa-pencil-alt"></i> Izmijeni
                                        </a>
                                        <button class="btn btn-danger" onclick="deleteOwner({{$owner->id}})">
                                            <i class="fas fa-trash"></i> Izbriši
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                        @else
                            <tr>
                                <td colspan="7" class="text-center">
                                    <p><b>Trenutno nema vlasnika</b></p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{ $owners->links() }}
@endsection

@section('scripts')
    <script src="{{asset('js/functions.js')}}"></script>
@endsection
