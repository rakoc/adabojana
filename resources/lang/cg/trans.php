<?php

return [
    'nav-home' => 'Početna',
    'nav-house' => 'Sve kuće',
    'nav-contact' => 'Kontakt',
    'nav-phone' => 'Telefon',
    'guests' => 'Gosti',
    'bedrooms' => 'Sobe',
    'search-houses' => 'Pretraži kuće',
    'ph-name' => 'Unesi ime',
    'ph-guest' => 'Gosti',
    'ph-price' => 'Cijena po noćenju',
    'ph-rooms' => 'Sobe',
    'button-search' => 'Pretraži',
    'find-your-house' => 'Pronadji kuću iz snova',
    'how-it-works' => 'Kako platforma radi ?',
    'icon-search' => 'Pretraži',
    'icon-search-desc' => 'Pretraži kuće na našoj platformi.',
    'icon-find' => 'Pronadji kuću',
    'icon-find-desc' => 'Odaberi kuću koja ti najviše odgovara',
    'icon-contact' => 'Kontaktiraj nas',
    'icon-contact-desc' => 'Pošalji zahtjev i uradićemo sve za tebe.',
    'top-rang' => 'Most popular',
    'top-choice' => 'Najbolji izbor',
    'button-view-all' => 'Pregledaj sve',
    'price' => 'Cijena po noćenju',
    'newsletter' => 'Prijavi se na naš newsletter i budi obaviješten o najnovijim ponudama',
    'social-media' => 'Društvene mreže',
    'contact' => 'Kontakt',
    'send-query' => 'Pošalji upit',
    'contact-title' => 'Kontaktiraj nas',
    'contact-name' => 'Ime i prezime',
    'contact-message' => 'Poruka',
    'contact-phone' => 'Broj telefona',
    'contact-send-message' => 'Pošalji poruku',
    'contact-desc' => 'Ako ne možeš da pronadješ smještaj sam, kontaktiraj nas i mi ćemo ga pronaći za tebe.'
];
