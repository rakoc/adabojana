function toast(type, message) {
    Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000
    })
    .fire({
        icon: type,
        title: message
    });
}

function swalDeleteHouse(url, id) {
    Swal.fire({
        title: 'Deaktivacija?',
        text: 'Da li ste sigurni da želite da deaktivirate kuću?',
        icon: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#06c258',
        confirmButtonText: 'U redu',
    }).then((result) => {
        if (result.value) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url + '/' + id,
                method: 'get',
                contentType: false,
                processData: false
            }).done(function () {
                toast('success', 'Uspješno deaktivirana kuća!');
                setTimeout(location.reload.bind(location), 2000);
            })
                .fail(function (    ) {
                    toast('error', 'Došlo je do greške prilikom deaktivacije kuće');
                });
        }
    });
}

