function saveOwner() {

    var data = new FormData;

    data.append('name', $('#name').val());
    data.append('email', $('#email').val());
    data.append('phone', $('#phone').val());
    data.append('password', $('#password').val());

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/admin/owners/create',
        method: 'POST',
        data: data,
        contentType: false,
        processData: false
    })
        .done(function () {
            toast('success', 'Uspješno ste kreirali novog vlasnika.');
            setTimeout(function () {
                location.href = '/admin/owners'
            }, 2000);
        })
        .fail(function (response) {
            var message = JSON.parse(response.responseText).errors;
            toast('error', String(message[Object.keys(message)[0]]));
        });
}

function setupAjax() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

function updateOwner(id) {

    var data = new FormData;

    data.append('name', $('#name').val());
    data.append('email', $('#email').val());
    data.append('phone', $('#phone').val());
    data.append('password', $('#password').val());

    setupAjax();
    $.ajax({
        url: '/admin/owners/' + id + '/update',
        method: 'POST',
        data: data,
        contentType: false,
        processData: false
    })
        .done(function () {
            toast('success', 'Uspješno ste izmijenili vlasnika.');
            setTimeout(function () {
                location.href = '/admin/owners'
            }, 2000);
        })
        .fail(function (response) {
            console.log(response);
            var message = JSON.parse(response.responseText).errors;
            toast('error', String(message[Object.keys(message)[0]]));
        });
}

function deleteOwner(id) {
    Swal.fire({
        title: 'Deaktivacija?',
        text: 'Da li ste sigurni da želite da izbrišete vlasnika?',
        icon: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#06c258',
        confirmButtonText: 'U redu',
    }).then((result) => {
        if (result.value) {
            setupAjax();
            $.ajax({
                url: '/admin/owners/' + id + '/delete',
                method: 'delete',
                contentType: false,
                processData: false
            }).done(function () {
                toast('success', 'Uspješno izbrisan vlasnik!');
                setTimeout(location.reload.bind(location), 2000);
            })
                .fail(function () {
                    toast('error', 'Došlo je do greške prilikom brisanja vlasnika.');
                });
        }
    });
}

function reservationApprove(id) {

    setupAjax();
    $.ajax({
        url: '/approve-reservation/' + id,
        method: 'get',
        processData: false,
        contentType: false
    })
        .done(function () {
            toast('success', 'Uspješno ste odobrili rezervaciju!');
            setTimeout(function () {
                location.reload()
            }, 3000)
        })
        .fail(function () {
            toast('error', 'Došlo je do greške. Pokušajte ponovo');
        })
}
