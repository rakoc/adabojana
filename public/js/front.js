function setupAjax() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

function sendContact() {

    var data = new FormData($('#contact-form')[0]);

    setupAjax()
    $.ajax({
        url: '/send-contact-form',
        method: 'post',
        data: data,
        contentType: false,
        processData: false
    })
        .done(function (response) {
            $('#contact-form')[0].reset();
            if (response[0] === 'en') {
                Swal.fire({
                    title: 'Success!',
                    text: 'You have successfully sent a contact message! We will contact you soon.',
                    icon: 'success',
                    confirmButtonColor: '#2cbdb8',
                    timer: 3500
                })
            } else {
                Swal.fire({
                    title: 'Uspjeh!',
                    text: 'Uspješno ste poslali kontakt poruku! Uskoro ćemo vas kontaktirati.',
                    icon: 'success',
                    confirmButtonColor: '#2cbdb8',
                    timer: 3500
                })
            }
        })
        .fail(function (error) {
            if (error.status === 422) {
                var message = JSON.parse(error.responseText).errors
                Swal.fire({
                    text: String(message[Object.keys(message)[0]]),
                    icon: 'error',
                    timer: 2000,
                    confirmButtonColor: '#2cbdb8',
                })
            } else {
                if (error.data[0] === 'en')
                Swal.fire({
                    title: 'Error!',
                    text: 'Error!',
                    icon: 'error',
                    timer: 2000,
                    confirmButtonColor: '#2cbdb8'
                })
            }
        })
}
