<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
|                               Public Routes
|--------------------------------------------------------------------------
*/
Route::get('/set-locale/{locale}', function ($locale) {
    if (! in_array($locale, ['en', 'cg'])) {
        abort(400);
    }
    App::setLocale($locale);
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('/clear-sessions', function () {
    Session::flush();
    return back();
});

Route::get('/', 'WebsiteController@homepageView');
Route::get('/houses', 'WebsiteController@housesView');
Route::get('/contact-us', 'WebsiteController@contactView');
Route::get('/house/{id}', 'WebsiteController@houseView');

Route::post('/send-contact-form', 'WebsiteController@sendMessage');
/*
|--------------------------------------------------------------------------
|                               Owner Routes
|--------------------------------------------------------------------------
*/

// Requests
Route::get('/my-requests', 'OwnerController@requestsView');
Route::get('/see-request/{id}', 'OwnerController@seeRequest');

// Houses
Route::get('/my-houses', 'OwnerController@housesView');
Route::get('/change-my-house/{id}', 'OwnerController@changeHouse');
Route::get('/delete-house/{id}', 'OwnerController@deactivateHouse');
Route::post('/update-my-house', 'OwnerController@updateHouse');

Route::get('/approve-reservation/{id}', 'OwnerController@approveReservation');
// Stats



//Admin Houses
Route::group(["prefix" => "admin", "middleware" => "admin"], function () {

    Route::get('/houses', 'AdminController@getAllActiveHouses');
    Route::get('/houses/inactive', 'AdminController@getAllInactiveHouses');
    Route::get('/houses/{id}/edit', 'AdminController@editHouse');
    Route::get('/delete-house/{id}', 'AdminController@deleteHouse');
    Route::post('/houses/update', 'AdminController@updateHouse');
    Route::get('/house', 'AdminController@createHouse');
    Route::post('/houses/create', 'AdminController@storeHouse');
    Route::get('/owners', 'AdminController@getAllOwners');
    Route::get('/owners/create', 'AdminController@createOwner');
    Route::post('owners/create', 'AdminController@storeOwner');
    Route::get('/owners/{id}/update', 'AdminController@editOwner');
    Route::post('/owners/{id}/update', 'AdminController@updateOwner');
    Route::delete('/owners/{id}/delete', 'AdminController@deleteOwner');

    Route::get('messages', 'AdminController@getAllMessages');
    Route::get('/see-message/{id}', 'AdminController@seeMessage');

    Route::get('/requests', 'AdminController@getAllReservations');
    Route::get('/see-request/{id}', 'AdminController@viewReservation');
});








Auth::routes(['register' => false]);

Route::get('/dashboard', 'HomeController@index')->name('home');
