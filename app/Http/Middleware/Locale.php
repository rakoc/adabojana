<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $raw_locale = session('locale');
        if (in_array($raw_locale, ['cg', 'en'])) {
            $locale = $raw_locale;
        } else {
            $locale = 'cg';
        }
        App::setLocale($locale);
        Session::put('locale', $locale);
        return $next($request);
    }
}
