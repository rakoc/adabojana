<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateHouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'houseName' => 'required|string|max:100',
            'housePrice' => 'required|numeric',
            'houseAddress' => 'required|string|max:150',
            'houseGuests' => 'required|numeric',
            'houseRooms' => 'required|numeric',
            'houseDescription' => 'required|string|max:1000',
            'lat' => 'required',
            'lng' => 'required',
            'coverImage' => 'mimes:jpg,jpeg,png|max:4096',
            'images.*' => 'mimes:jpg,jpeg,png',
            'attributes' => 'required',
            'translations' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'ID potreban',
            'houseName.required' => 'Molimo vas unesite naziv kuće',
            'houseName.max' => 'Maksimalna dužina naziva kuće je 100 karaktera',
            'housePrice.required' => 'Molimo vas unesite cijenu noćenja',
            'housePrice.numeric' => 'Cijena mora biti broj.',
            'houseRooms' => 'Molimo vas unesite broj soba.',
            'houseGuests' => 'Molimo vas unesite broj gosti',
            'houseDescription.required' => 'Molimo vas unesite opis kuće',
            'houseDescription.max' => 'Maksimalna dužina opisa kuće je 1000 karaktera',
            'lat.required' => 'Molimo vas dodajte pin na mapu',
            'lng.required' => 'Molimo vas dodajte pin na mapu',
            'coverImage.mimes' => 'Naslovna slika može biti u formatu JPG, JPEG, PNG',
            'coverImages.max' => 'Maksimalna veličina naslovne slike je 4MB',
            'images.*.mimes' => 'Slike moraju biti u formatu JPG, JPEG, PNG',
            'attributes.required' => 'Molimo vas unesite potrebne atribute',
            'translations.required' => 'Molimo vas unesite potrebne prevode'
        ];
    }
}
