<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class ContactMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone_number' => 'required|regex: #^[+]{0,1}[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$#',
            'message' => 'required|max:2000',
            'message_status_id' => 'required'
        ];
    }

    public function messages()
    {
        if (Session::get('locale') == 'en') {
            return [
                'name.required' => 'Please enter your name.',
                'name.max' => 'The name can contain maximum 255 characters.',
                'email.required' => 'Please enter your email.',
                'email.email' => 'The email is not in a valid format.',
                'email.max' => 'The email can contain maximum 255 characters.',
                'phone_number.required' => 'Please enter your phone number.',
                'phone_number.regex' => 'The phone number is not in a valid format.',
                'message.required' => 'Please enter a message.',
                'message.max' => 'The message can contain maximum 2000 characters.'
            ];
        } else {
            return [
                'name.required' => 'Molimo Vas unesite ime i prezime.',
                'name.max' => 'Ime i prezime može sadržati najviše 255 karaktera.',
                'email.required' => 'Molimo Vas unesite email.',
                'email.email' => 'Email nije u validnom formatu.',
                'email.max' => 'Ime i prezime može sadržati najviše 255 karaktera.',
                'phone_number.required' => 'Molimo Vas unesite broj telefona.',
                'phone_number.regex' => 'Broj telefona nije u validnom formatu.',
                'message.required' => 'Molimo Vas unesite poruku.',
                'message.max' => 'Ime i prezime može sadržati najviše 255 karaktera.'
            ];
        }
    }
}
