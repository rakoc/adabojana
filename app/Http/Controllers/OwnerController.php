<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\House;
use App\HouseAttribute;
use App\HouseImage;
use App\HouseTranslation;
use App\Language;
use App\Request as Requests;
use App\RequestStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OwnerController extends Controller
{

    /**
     * Method to get requests view for owners.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function requestsView()
    {
        if (Auth::check()) {
            if (sizeof(Auth::user()->house) > 0) {
                $requests = Requests::getRequestsByAuthUser();
                return view('admin.requests.index', compact('requests'));
            } else {
                $requests = null;
                return view('admin.requests.index', compact('requests'));
            }

        } else {
            abort(403, 'Access is forbidden!');
        }
    }

    /**
     *  Method to view request and write reply.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function seeRequest($id)
    {
        $request = Requests::findOrFail($id);
        if($request->requestStatus->code == 'WAITING')
            $request->request_status_id = RequestStatus::where('code', 'SEEN')->first()->id;
            $request->save();
            $request->load('requestStatus');
        return view('admin.requests.show', compact('request'));
    }

    public function approveReservation($id)
    {
        $req = Requests::findOrFail($id);
        $req->request_status_id = RequestStatus::where('code', RequestStatus::APPROVED)->first()->id;
        $req->save();

        return response()->json(['success'], 200);
    }
    /**
     *  Method to get houses view for owners.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function housesView()
    {
        $houses = House::where('owner', Auth::id())->where('active', true)->paginate(10);
        return view('admin.houses.index', compact('houses'));
    }


    /**
     * Method to get edit view of house for owners.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changeHouse($id)
    {
        $house = House::whereId($id)->whereActive(true)->first();
        $attributes = Attribute::all();
        $languages = Language::where('active', true)->where('code', '!=', 'MNE')->get();
        $users = User::where('active', true)->get();
        return view('admin.houses.edit', compact('house', 'attributes', 'users', 'languages'));
    }

    /**
     * Method to deactivate house.
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function deactivateHouse($id)
    {
        if ($id != '' && $id != null) {
            $house = House::findOrFail($id);
            $house->active = false;
            $house->save();
            return response('success', 200);
        } else {
            abort('401', 'Bad Parameter');
        }
    }


    public function updateHouse(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'houseName' => 'required|string|max:100',
            'housePrice' => 'required|numeric',
            'houseAddress' => 'required|string|max:150',
            'houseDescription' => 'required|string|max:1000',
            'houseRooms' => 'required|numeric',
            'houseGuests' => 'required|numeric',
            'lat' => 'required',
            'lng' => 'required',
            'coverImage' => 'mimes:jpg,jpeg,png|max:4096',
            'images.*' => 'mimes:jpg,jpeg,png',
            'attributes' => 'required',
            'translations' => 'required'
        ]);

        $house = House::findOrFail($request->id);
        $house->name = $request->houseName;
        $house->price = $request->housePrice;
        $house->address = $request->houseAddress;
        $house->rooms = $request->houseRooms;
        $house->guests = $request->houseGuests;
        $house->description = $request->houseDescription;
        $house->latitude = $request->lat;
        $house->longitude = $request->lng;

        if($request->hasFile('coverImage')) {
            $destinationPath = '/images/house/';
            $imageName = $request->file('coverImage')->getClientOriginalName();
            $filenameCover = time() . str_replace(' ', '_', $imageName);
            $request->file('coverImage')->move(public_path() . $destinationPath, $filenameCover);
            $house->image = $destinationPath . $filenameCover;
        }

        if($request->hasFile('images')) {
            $oldPhotos = HouseImage::where('active', true)->where('house_id', $house->id)->get();
            foreach ($oldPhotos as $p) {
                $p->active = false;
                $p->save();
            }

            foreach ($request->file('images') as $image) {
                $destinationPath = '/images/house/';
                $imageName = $image->getClientOriginalName();
                $filenameCover = time() . str_replace(' ', '_', $imageName);;
                $image->move(public_path() . $destinationPath, $filenameCover);

                $newHouseImage = new HouseImage();
                $newHouseImage->image = $destinationPath . $filenameCover;
                $newHouseImage->house_id = $house->id;
                $newHouseImage->active = true;
                $newHouseImage->save();
            }
        }

        $attributesRequest = $request->get('attributes');
        if (sizeof($attributesRequest) > 0) {
            $currentAttributes = HouseAttribute::where('house_id', $house->id)->get();

            foreach ($attributesRequest as $attr) {
                $decodedCurr = json_decode($attr);
                $found = false;
                foreach ($currentAttributes as $curr) {
                    if ($decodedCurr[0] == $curr->attribute->name) {
                        $found = true;
                        $curr->value = $decodedCurr[1] == 'true' ? true : false;
                        $curr->value_text = $decodedCurr[2];
                        $curr->save();
                    }
                }
                if (!$found) {
                    $newHA = new HouseAttribute();
                    $newHA->house_id = $house->id;
                    $newHA->attribute_id = Attribute::where('name', trim($decodedCurr[0]))->first()->id;
                    $newHA->value = $decodedCurr[1] == 'true' ? true : false;
                    $newHA->value_text = $decodedCurr[2];
                    $newHA->save();
                }
            }
        }


        $translationsRequest = $request->get('translations');
        if (sizeof($translationsRequest) > 0) {
            $currentTranslations = HouseTranslation::where('house_id', $house->id)->get();

            foreach ($translationsRequest as $trans) {
                $decodedTrans = json_decode($trans);
                $found = false;
                foreach ($currentTranslations as $curr) {
                    if ($decodedTrans[0] == $curr->language->code) {
                        $found = true;
                        $curr->name = $decodedTrans[1];
                        $curr->description = $decodedTrans[2];
                        $curr->save();
                    }
                }
                if (!$found) {
                    $newTrans = new HouseTranslation();
                    $newTrans->house_id = $house->id;
                    $newTrans->language_id = Language::where('code', trim($decodedTrans[0])->first()->id);
                    $newTrans->name = $decodedTrans[1];
                    $newTrans->description = $decodedTrans[2];
                    $newTrans->save();
                }
            }
        }


        $house->save();
        return response('Success', 200);
    }
}
