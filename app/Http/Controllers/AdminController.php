<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\House;
use App\HouseAttribute;
use App\HouseImage;
use App\HouseTranslation;
use App\Http\Requests\CreateHouseRequest;
use App\Http\Requests\UpdateHouseRequest;
use App\Language;
use App\Message;
use App\MessageStatus;
use App\Request as Requests;
use App\Request as Reservation;
use App\RequestStatus;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///                                                    OWNERS
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getAllOwners()
    {
        $owners = User::where('role_id', Role::where('name', 'owner')->first()->id)->whereActive(true)->paginate(10);
        return view('admin.owners.index', compact('owners'));
    }

    public function createOwner()
    {
        return view('admin.owners.create');
    }

    public function storeOwner(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:30',
            'password' => 'required|max:32'
        ], [
            'name.required' => 'Molimo vas unesite ime.',
            'name.max' => 'Ime može sadržati najviše 255 karaktera.',

            'email.required' => 'Molimo vas unesite email.',
            'email.email' => 'Email nije u odgovarajućem formatu.',
            'email.max' => 'Email može sadržati najviše 255 karaktera.',

            'phone.required' => 'Molimo vas unesite broj telefona.',
            'phone.max' => 'Broj telefona može sadržati najviše 30 karaktera.',

            'password.required' => 'Molimo vas unesite lozinku.',
            'password.max' => 'Lozinka može sadržati najviše 32 karaktera.',

        ]);

        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phone;
        $user->role_id = Role::where('name', 'owner')->first()->id;
        $user->password = Hash::make($request->password);
        $user->active = true;
        $user->save();

        return response()->json(['success'], 200);
    }

    public function editOwner($id)
    {
        if ($id != '') {
            $owner = User::where('id', $id)->where('active', true)->first();
            return view('admin.owners.edit', compact('owner'));
        } else {
            abort(404, 'ID nije validan');
        }
    }

    public function updateOwner(Request $request, $id)
    {
        if ($id != '') {

            $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|email|max:255',
                'phone' => 'required|max:30',
                'password' => 'nullable|max:32'
            ], [
                'name.required' => 'Molimo vas unesite ime.',
                'name.max' => 'Ime može sadržati najviše 255 karaktera.',

                'email.required' => 'Molimo vas unesite email.',
                'email.email' => 'Email nije u odgovarajućem formatu.',
                'email.max' => 'Email može sadržati najviše 255 karaktera.',

                'phone.required' => 'Molimo vas unesite broj telefona.',
                'phone.max' => 'Broj telefona može sadržati najviše 30 karaktera.',

                'password.max' => 'Lozinka može sadržati najviše 32 karaktera.',
            ]);

            $owner = User::where('id', $id)->where('active', true)->first();

            if ($owner) {

                $owner->name = $request->name;
                $owner->email = $request->email;
                $owner->phone_number = $request->phone;
                if ($request->password != null || $request->password != '') {
                    $owner->password = Hash::make($request->password);
                }
                $owner->save();

                return response()->json(['success'], 200);
            } else {
                return response()->json(['Korisnik nije pronadjen'], 404);
            }
        } else {
            return response()->json(['ID korisnika nije validan'], 503);
        }
    }

    public function deleteOwner($id)
    {
        if ($id != '') {
            $owner = User::where('id', $id)->whereActive(true)->first();
            if ($owner) {
                $owner->update(['active' => false]);
                return response()->json(['success'], 200);
            }
        } else {
            return response()->json(['ID nije validan'], 404);
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///                                                   HOUSES
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function getAllActiveHouses()
    {
        $houses = House::where('active', true)->orderBy('id')->paginate(10);
        return view('admin.houses.index', compact('houses'));
    }

    public function getAllInactiveHouses()
    {
        $houses = House::where('active', true)->orderBy('id')->paginate(10);
        return view('admin.houses.index', compact('houses'));
    }

    public function editHouse($id)
    {
        $attributes = Attribute::all();
        $languages = Language::where('active', true)->where('code', '!=', 'MNE')->get();
        $users = User::where('active', true)->get();
        $house = House::whereId($id)->whereActive(true)->first();
        return view('admin.houses.edit', compact('house', 'attributes', 'languages', 'users'));
    }

    public function deleteHouse($id)
    {
        if ($id != '' && $id != null) {
            $house = House::findOrFail($id);
            $house->active = false;
            $house->save();
            return response('success', 200);
        } else {
            abort('422', 'Bad Parameter');
        }
    }

    public function updateHouse(UpdateHouseRequest $request)
    {
        $valid = $request->validated();

        $house = House::findOrFail($request->id);
        $house->name = $valid['houseName'];
        $house->price = $valid['housePrice'];
        $house->guests = $valid['houseGuests'];
        $house->rooms = $valid['houseRooms'];
        $house->address = $valid['houseAddress'];
        $house->description = $valid['houseDescription'];
        $house->latitude = $valid['lat'];
        $house->longitude = $valid['lng'];

        if ($request->hasFile('coverImage')) {
            $destinationPath = '/images/house/';
            $imageName = $request->file('coverImage')->getClientOriginalName();
            $filenameCover = time() . str_replace(' ', '_', $imageName);;
            $request->file('coverImage')->move(public_path() . $destinationPath, $filenameCover);
            $house->image = $destinationPath . $filenameCover;
        }

        if ($request->hasFile('images')) {
            $oldPhotos = HouseImage::where('active', true)->where('house_id', $house->id)->get();
            foreach ($oldPhotos as $p) {
                $p->active = false;
                $p->save();
            }

            foreach ($request->file('images') as $image) {
                $destinationPath = '/images/house/';
                $imageName = $image->getClientOriginalName();
                $filenameCover = time() . str_replace(' ', '_', $imageName);;
                $image->move(public_path() . $destinationPath, $filenameCover);

                $newHouseImage = new HouseImage();
                $newHouseImage->image = $destinationPath . $filenameCover;
                $newHouseImage->house_id = $house->id;
                $newHouseImage->active = true;
                $newHouseImage->save();
            }
        }

        $attributesRequest = $request->get('attributes');
        if (sizeof($attributesRequest) > 0) {
            $currentAttributes = HouseAttribute::where('house_id', $house->id)->get();

            foreach ($attributesRequest as $attr) {
                $decodedCurr = json_decode($attr);
                $found = false;
                foreach ($currentAttributes as $curr) {
                    if ($decodedCurr[0] == $curr->attribute->name) {
                        $found = true;
                        $curr->value = $decodedCurr[1] == 'true' ? true : false;
                        $curr->value_text = $decodedCurr[2];
                        $curr->save();
                    }
                }
                if (!$found) {
                    $newHA = new HouseAttribute();
                    $newHA->house_id = $house->id;
                    dd($decodedCurr[0]);
                    $newHA->attribute_id = Attribute::where('name', trim($decodedCurr[0]))->first()->id;
                    $newHA->value = $decodedCurr[1] == 'true' ? true : false;
                    $newHA->value_text = $decodedCurr[2];
                    $newHA->save();
                }
            }
        }


        $translationsRequest = $request->get('translations');
        if (sizeof($translationsRequest) > 0) {
            $currentTranslations = HouseTranslation::where('house_id', $house->id)->get();

            foreach ($translationsRequest as $trans) {
                $decodedTrans = json_decode($trans);
                $found = false;
                foreach ($currentTranslations as $curr) {
                    if ($decodedTrans[0] == $curr->language->code) {
                        $found = true;
                        $curr->name = $decodedTrans[1];
                        $curr->description = $decodedTrans[2];
                        $curr->save();
                    }
                }
                if (!$found) {
                    $newTrans = new HouseTranslation();
                    $newTrans->house_id = $house->id;
                    $newTrans->language_id = Language::where('code', trim($decodedTrans[0]));
                    $newTrans->name = $decodedTrans[1];
                    $newTrans->description = $decodedTrans[2];
                    $newTrans->save();
                }
            }
        }


        $house->save();
        return response('Success', 200);
    }

    public function createHouse()
    {
        $attributes = Attribute::all();
        $languages = Language::where('active', true)->where('code', '!=', 'MNE')->get();
        $users = User::where('active', true)->get();
        return view('admin.houses.create', compact('attributes', 'languages', 'users'));
    }


    public function storeHouse(CreateHouseRequest $request)
    {
        $valid = $request->validated();

        $house = new House();
        $house->name = $valid['houseName'];
        $house->price = $valid['housePrice'];
        $house->address = $valid['houseAddress'];
        $house->guests = $valid['houseGuests'];
        $house->rooms = $valid['houseRooms'];
        $house->description = $valid['houseDescription'];
        $house->latitude = $valid['lat'];
        $house->longitude = $valid['lng'];
        $house->owner = $valid['owner'];
        $house->updated_by = $request->owner;

        if ($request->hasFile('coverImage')) {
            $destinationPath = '/images/house/';
            $imageName = $request->file('coverImage')->getClientOriginalName();
            $filenameCover = time() . str_replace(' ', '_', $imageName);;
            $request->file('coverImage')->move(public_path() . $destinationPath, $filenameCover);
            $house->image = $destinationPath . $filenameCover;
        }
        $house->save();
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $destinationPath = '/images/house/';
                $imageName = $image->getClientOriginalName();
                $filenameCover = time() . str_replace(' ', '_', $imageName);;
                $image->move(public_path() . $destinationPath, $filenameCover);

                $newHouseImage = new HouseImage();
                $newHouseImage->image = $destinationPath . $filenameCover;
                $newHouseImage->house_id = $house->id;
                $newHouseImage->active = true;
                $newHouseImage->save();
            }
        }

        $attributesRequest = $request->get('attributes');
        if (sizeof($attributesRequest) > 0) {

            foreach ($attributesRequest as $attr) {
                $decodedCurr = json_decode($attr);
                $newHA = new HouseAttribute();
                $newHA->house_id = $house->id;
                $newHA->attribute_id = Attribute::where('name', trim($decodedCurr[0]))->first()->id;
                $newHA->value = $decodedCurr[1] == 'true' ? true : false;
                $newHA->value_text = $decodedCurr[2];
                $newHA->save();
            }
        }

        $translationsRequest = $request->get('translations');
        if (sizeof($translationsRequest) > 0) {

            foreach ($translationsRequest as $trans) {
                $decodedTrans = json_decode($trans);
                $newTrans = new HouseTranslation();
                $newTrans->house_id = $house->id;
                $newTrans->language_id = Language::where('code', trim($decodedTrans[0]))->first()->id;
                $newTrans->name = $decodedTrans[1];
                $newTrans->description = $decodedTrans[2];
                $newTrans->save();
            }

        }
        return response('Success', 200);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///                                                   MESSAGES
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function getAllMessages()
    {
        $messages = Message::orderBy('message_status_id', 'desc')->paginate(10);
        return view('admin.messages.index', compact('messages'));
    }


    public function seeMessage($id)
    {
        $message = Message::findOrFail($id);
        if ($message->messageStatus->code == MessageStatus::WAITING) {
            $message->message_status_id = MessageStatus::whereCode('SEEN')->first()->id;
            $message->save();
            $message->load('messageStatus');
        }
        return view('admin.messages.show', compact('message'));
    }

    public function messageReply()
    {
        // todo napraviti odgovor mail
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///                                                RESERVATIONS
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function getAllReservations()
    {
        $requests = Reservation::orderBy('request_status_id', 'asc')->paginate(10);
        return view('admin.requests.index', compact('requests'));
    }

    public function viewReservation($id)
    {
        $request = Reservation::findOrFail($id);
        return view('admin.requests.show', compact('request'));
    }
}
