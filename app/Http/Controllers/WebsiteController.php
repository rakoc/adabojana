<?php

namespace App\Http\Controllers;

use App\House;
use App\Http\Requests\ContactMessage;
use App\Message;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{

    public function homepageView()
    {
        $slider = House::where('active', true)->orderBy('id', 'desc')->take(3)->get();
        $best = House::where('active', true)->orderBy('id', 'desc')->take(1)->get();
        return view('index', compact('best', 'slider'));
    }

    public function housesView(Request $request)
    {
        $name = $request->name;
        $rooms = $request->rooms;
        $guests = $request->guests;
        $price = $request->price;

        $houses = House::where('active', true)
            ->where(function ($query) use ($name) {
                if ($name != '') {
                    $query->where('name', 'like', '%' . $name . '%');
                }
            })
            ->where(function ($query) use ($rooms) {
                if ($rooms != '') {
                    $query->where('rooms', '>=', $rooms);
                }
            })
            ->where(function ($query) use ($guests) {
                if ($guests != '') {
                    $query->where('guests', '>=', $guests);
                }
            })
            ->where(function ($query) use ($price) {
                if ($price != '') {
                    $query->where('');
                }
            })
            ->paginate(5);
        return view('houses', compact($houses));
    }

    public function contactView()
    {
        return view('contact');
    }

    public function houseView($id)
    {
        if ($id != '') {
            $house = House::where('active', true)->whereId($id)->first();
            if ($house) {
                return view('house', compact('house'));
            } else {
                abort(404, 'Kuća nije pronadjena!');
            }
        } else {
            abort(404, 'Bad argument');
        }
    }


    public function sendMessage(ContactMessage $request)
    {
        $validated = $request->validated();
        Message::create(collect($validated)->toArray(), 'message_status_id', 1);

        return response()->json([session('locale')],200);
    }
}
