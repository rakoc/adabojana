<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseTranslation extends Model
{
    protected $guarded = [];

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function house()
    {
        return $this->belongsTo(House::class, 'house_id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
