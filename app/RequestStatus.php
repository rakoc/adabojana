<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestStatus extends Model
{
    protected $guarded = [];
    const WAITING = 'WAITING';
    const SEEN = 'SEEN';
    const REPLIED = 'REPLIED';
    const APPROVED = 'APPROVED';


    public function requests()
    {
        return $this->hasMany(Request::class);
    }

}
