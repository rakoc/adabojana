<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $guarded = [];

    public function houseAttributes()
    {
        return $this->hasMany(HouseAttribute::class);
    }

    public function translations()
    {
        return $this->hasMany(AttributeTranslation::class);
    }
}
