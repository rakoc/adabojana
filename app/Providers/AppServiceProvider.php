<?php

namespace App\Providers;

use App\Message;
use App\MessageStatus;
use App\Request;
use App\RequestStatus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layouts.admin'], function ($view) {

            $newReservations = 0;
            $newMessages = 0;

           if (Auth::user()->role->name == 'owner') {
              $newReservations = Request::where('request_status_id', RequestStatus::where('code', RequestStatus::WAITING)->first()->id)->count();
           }

            if (Auth::user()->role->name == 'admin' || Auth::user()->role->name == 'head') {
                $newMessages = Message::where('message_status_id', MessageStatus::where('code', MessageStatus::WAITING)->first()->id)->count();
            }

            $view->with('reservations', $newReservations)
                ->with('messages', $newMessages);
        });
    }
}
