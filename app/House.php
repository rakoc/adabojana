<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    protected $guarded = [];

    public function houseAttributes()
    {
        return $this->hasMany(HouseAttribute::class);
    }

    public function houseImages()
    {
        return $this->hasMany(HouseImage::class);
    }

    public function translations()
    {
        return $this->hasMany(HouseTranslation::class);
    }

    public function requests()
    {
        return $this->hasMany(Request::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }


}
