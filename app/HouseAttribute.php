<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseAttribute extends Model
{
    protected $guarded = [];

    public function house()
    {
        return $this->belongsTo(House::class, 'house_id');
    }

    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }
}
