<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];

    public function messageStatus()
    {
        return $this->belongsTo(MessageStatus::class, 'message_status_id');
    }
}
