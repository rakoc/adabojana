<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Request extends Model
{
    protected $guarded = [];

    public function house()
    {
        return $this->belongsTo(House::class, 'house_id', 'id');
    }

    public function requestStatus()
    {
        return $this->belongsTo(RequestStatus::class, 'request_status_id');
    }


    public static function getRequestsByAuthUser()
    {
        return Request::whereIn('house_id', Auth::user()->house->pluck('id'))
            ->where('active', true)->orderBy('request_status_id')
            ->paginate(20);
    }
}
