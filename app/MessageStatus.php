<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageStatus extends Model
{
    const APPROVED = 'APPROVED';
    const WAITING = 'WAITING';
    const SEEN = 'SEEN';

    protected $guarded = [];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
