<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseImage extends Model
{
    protected $guarded = [];

    public function house()
    {
        return $this->belongsTo(House::class, 'house_id');
    }
}
