<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('request_statuses')->insert([
            [
                'code' => 'WAITING'
            ],
            [
                'code' => 'SEEN'
            ],
            [
                'code' => 'REPLIED'
            ],
            [
                'code' => 'APPROVED'
            ]
        ]);

        DB::table('message_statuses')->insert([
            [
                'code' => 'WAITING'
            ],
            [
                'code' => 'SEEN'
            ],
            [
                'code' => 'REPLIED'
            ]
        ]);
    }
}
