<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserAndRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'user'
            ],
            [
                'name' => 'owner'
            ],
            [
                'name' => 'admin'
            ],
            [
                'name' => 'head'
            ]
        ]);


        DB::table('users')->insert([
            [
                'name' => 'Luka',
                'email' => 'rakocevic.luka@gmail.com',
                'password' => Hash::make('luka211009'),
                'role_id' => '4'
            ],
            [
                'name' => 'Danilo Vukcevic',
                'email' => 'danilo@vukcevic.me',
                'password' => Hash::make('danilodanilo'),
                'role_id' => '3'
            ]
        ]);
    }
}
