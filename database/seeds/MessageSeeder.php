<?php

use App\Message;
use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Message::insert([[
            'name' => 'Ime iprezime',
            'email' => 'Email@test.com',
            'phone_number' => '0671234567',
            'message' => 'Poruka za apartman  nikola.',
            'message_status_id' => 1,
            'active' => true
        ]]);
    }
}
