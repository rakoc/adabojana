<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserAndRoleSeeder::class);
         $this->call(LanguageSeeder::class);
         $this->call(AttributeSeeder::class);
         $this->call(HouseSeeder::class);
         $this->call(RequestStatusSeeder::class);
         $this->call(RequestSeeder::class);
         $this->call(MessageSeeder::class);
    }
}
