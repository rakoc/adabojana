<?php

use Illuminate\Database\Seeder;

class RequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('requests')->insert([
            [
                'name' => 'Zoran Filipovic',
                'email' => 'test@email.com',
                'phone_number' => '056443123',
                'message' => 'Dugacka poruka. Dugacka poruka. Dugacka poruka. Dugacka poruka. Dugacka poruka. Dugacka poruka. ',
                'house_id' => 1,
                'request_status_id' => 1
            ],
            [
                'name' => 'Marko Markovic',
                'email' => 'marko@email.com',
                'phone_number' => '056443123',
                'message' => 'Dugacka poruka. Dugacka poruka. Dugacka poruka. Dugacka poruka. Dugacka poruka. Dugacka poruka. ',
                'house_id' => 2,
                'request_status_id' => 1
            ]
        ]);
    }
}
