<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('houses')->insert([
            [
                'name' => 'Villa Dakizi',
                'price' => 70,
                'address' => 'Ul. Neznanih junaka',
                'latitude' => '44.12313',
                'rooms' => 2,
                'guests' => 2,
                'longitude' => '19.24243',
                'description' => 'Set on the beachfront, the 5-star Boutique Hotel More offers elegant accommodations, an outdoor pool, 2 restaurants and the unique Cave Bar. Surrounded by lush Mediterranean vegetation, it is located on the Lapad Peninsula, only 2.2 mi from Dubrovnik’s Old Town. All rooms and suites consist of sea view or garden view and air conditioning.
                                    The flat-screen TV comes with free Pay-per-view and satellite channels.
                                    Each unit has a private bathroom with marble elements and a bathtub.
                                    Wellness & Beauty More consists of the fitness center and Hydro tub.
                                    Restaurant Tramuntana offers local Dalmatian dishes based on the best selection of Adriatic sea food.
                                    Cave Bar More, located in a natural cave split into 3 levels beneath the hotel, features a spectacular setting where you can relax with a wide choice of beverages and snacks.
                                    Free Wi-fi is available throughout the property. Free parking is also provided on site. A path that stretches along the entire peninsula and passes right next to the property is ideal for walking, relaxing by the sea and enjoying pine tree shades.
                                    A local bus stop with frequent links to the UNESCO-protected historic center is in the immediate vicinity. Dubrovnik Bus Station and Ferry Port are 2.5 mi away, while Dubrovnik International Airport is 14 mi from the More Boutique Hotel.',
                'image' => 'hakuna-matata-01.jpg',
                'owner' => 1,
                'updated_by' => 1
            ],
            [
                'name' => 'Villa Dakizi',
                'price' => 70,
                'address' => 'Ul. Neznanih junaka',
                'latitude' => '44.12313',
                'longitude' => '19.24243',
                'rooms' => 2,
                'guests' => 2,
                'description' => 'Set on the beachfront, the 5-star Boutique Hotel More offers elegant accommodations, an outdoor pool, 2 restaurants and the unique Cave Bar. Surrounded by lush Mediterranean vegetation, it is located on the Lapad Peninsula, only 2.2 mi from Dubrovnik’s Old Town. All rooms and suites consist of sea view or garden view and air conditioning.
                                    The flat-screen TV comes with free Pay-per-view and satellite channels.
                                    Each unit has a private bathroom with marble elements and a bathtub.
                                    Wellness & Beauty More consists of the fitness center and Hydro tub.
                                    Restaurant Tramuntana offers local Dalmatian dishes based on the best selection of Adriatic sea food.
                                    Cave Bar More, located in a natural cave split into 3 levels beneath the hotel, features a spectacular setting where you can relax with a wide choice of beverages and snacks.
                                    Free Wi-fi is available throughout the property. Free parking is also provided on site. A path that stretches along the entire peninsula and passes right next to the property is ideal for walking, relaxing by the sea and enjoying pine tree shades.
                                    A local bus stop with frequent links to the UNESCO-protected historic center is in the immediate vicinity. Dubrovnik Bus Station and Ferry Port are 2.5 mi away, while Dubrovnik International Airport is 14 mi from the More Boutique Hotel.',
                'image' => 'hakuna-matata-01.jpg',
                'owner' => 1,
                'updated_by' => 1
            ]
        ]);
    }
}
