<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attributes')->insert([
            [
                'name' => 'parking'
            ],
            [
                'name' => 'wifi'
            ],
            [
                'name' => 'tv'
            ],
            [
                'name' => 'furniture'
            ],
            [
                'name' => 'terrace'
            ],
            [
                'name' => 'airconditioning'
            ],
            [
                'name' => 'kitchen'
            ],
            [
                'name' => 'barbecue'
            ]
        ]);

        DB::table('attribute_translations')->insert([
            // Crnogorski
            [
                'language_id' => 1,
                'attribute_id' => 1,
                'name' => 'Parking'
            ],
            [
                'language_id' => 2,
                'attribute_id' => 1,
                'name' => 'Parking'
            ],

            [
                'language_id' => 1,
                'attribute_id' => 2,
                'name' => 'WIFI'
            ],
            [
                'language_id' => 2,
                'attribute_id' => 2,
                'name' => 'WIFI'
            ],
            [
                'language_id' => 1,
                'attribute_id' => 3,
                'name' => 'TV'
            ],
            [
                'language_id' => 2,
                'attribute_id' => 3,
                'name' => 'TV'
            ],
            [
                'language_id' => 1,
                'attribute_id' => 4,
                'name' => 'Spoljni namještaj'
            ],
            [
                'language_id' => 2,
                'attribute_id' => 4,
                'name' => 'Outdoor furniture'
            ],

            [
                'language_id' => 1,
                'attribute_id' => 5,
                'name' => 'Terasa'
            ],
            [
                'language_id' => 2,
                'attribute_id' => 5,
                'name' => 'Terrace'
            ],
            [
                'language_id' => 1,
                'attribute_id' => 6,
                'name' => 'Klimatizovano'
            ],
            [
                'language_id' => 2,
                'attribute_id' => 6,
                'name' => 'Air conditioned'
            ],
            [
                'language_id' => 1,
                'attribute_id' => 7,
                'name' => 'Kuhinja'
            ],
            [
                'language_id' => 2,
                'attribute_id' => 7,
                'name' => 'Kitchen'
            ],
            [
                'language_id' => 1,
                'attribute_id' => 8,
                'name' => 'Roštilj'
            ],
            [
                'language_id' => 2,
                'attribute_id' => 8,
                'name' => 'Barbecue'
            ]
        ]);
    }
}
